import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { customer } from '../utils/customer.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  user;

  constructor(private nav: NavController, private auth: AuthService, public http: HttpClient, public storage: Storage) {
    this.getUsers();
    
  }

  public cres: any[] = [];

  public getUsers(){
    this.storage.get('age').then((val) => {
      this.auth.getAllUsers().subscribe(result => {
        let res = JSON.stringify(result);
        this.cres = JSON.parse(res);
        this.user = val;
        console.log(this.cres)

      });
    });
  }
    
    
  
}
