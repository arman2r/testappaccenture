import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AlertController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  //createSuccess = false;
  myDate; maxDate;
  public name: any;
  registerCredentials = { firstname: '', lastname: '', birthdate: '', identification: '' };

  constructor(
    public authService: AuthService,
    public router: Router,
    private alertCtrl: AlertController,
    private nav: NavController,
    private _Activatedroute: ActivatedRoute,
    private fb: FormBuilder) {
    this.createForm();
    this.maxDate = '2000-12-31';
  } 

  ngOnInit() {
   
  }

  createForm(){
    this.registerForm = this.fb.group ({
      firstname: ['', [Validators.required, Validators.minLength(3)]],
      lastname: ['', [Validators.required, Validators.minLength(3)]],
      birthdate: ['', [Validators.required, Validators.minLength(3)]],
      identification:  ['', [Validators.required, Validators.minLength(3)], this.isIdentification.bind(this) ]
    });
  }

  /*
  isIdentification( control: FormControl ): Promise<any>|Observable<any> {
    let promesa = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          this.authService.isIdentifacation(control.value).subscribe(() => {
            console.log();

            if (control.value === '1110515315') {
              resolve({existe: true});
            } else {
              resolve(null)
            }
          })
        }, 3000);
      }
    );
    return promesa;
  }
  */
  
  isIdentification(control: FormControl) {
    const q = new Promise((resolve, reject) => {
      setTimeout(() => {
        this.authService.isIdentifacation(control.value).subscribe(() => {
          //console.log(control.value)
          resolve(null);
        }, () => { resolve({ 'isIdentificationUnique': true }); });
      }, 1000);
    });
    return q;
  }

  setDate() {
    this.maxDate = this.myDate;
  }

  get f() { return this.registerForm.controls; }

  public onSubmit() {
    if (
      this.registerCredentials.firstname === null ||
      this.registerCredentials.lastname === null ||
      this.registerCredentials.birthdate === null ||
      this.registerCredentials.identification == null) {
      this.showPopup('Error', 'Complete todos los campos para continuar.');
    } else {
      this.authService.register(name, this.registerCredentials).subscribe(success => {
        if (success) {
          this.submitted = true;
          console.log(name);
          this.showPopup('Success', 'Cuenta creada exitosamente.');
        } else {
          this.showPopup("Error", "Poseemos problemas al registrar usuario , intente de nuevo porfavor.");
        }
      },
        error => {
          this.showPopup("Error", error);
        });
    }
  }

  
  async showPopup(title, text) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: text,
      buttons: [{
        text: 'OK',
        handler: data => {
          if (this.submitted){
            this.router.navigate(['/home'])
          }
        }
      }]
    });

    await alert.present();
  }
}
