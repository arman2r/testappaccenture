import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers } from '@angular/http';
import { map, catchError, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { customer } from '../utils/customer.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
    // Change to this http://ed43bb3b.ngrok.io/api/login
    static readonly LOGIN_URL = 'https://testbankapi.firebaseio.com/clients.json';
    // Change to this http://ed43bb3b.ngrok.io/api/register
    static readonly REGISTER_URL = 'https://testbankapi.firebaseio.com/clients.json';
    access: boolean;
    token: string;    
  
    constructor(public http: HttpClient, public storage: Storage ) {
      
    }
  
    // Login
    public login(credentials) {
      if (credentials.email === null || credentials.password === null) {
        return Observable.throw("Please insert credentials.");
      } else {
        return Observable.create(observer => {
  
          this.http.post(AuthService.LOGIN_URL, credentials)
          .pipe(map((res: any) => res.json()))
          .subscribe( data => {
            if (data.access_token) {
              this.token = 'Bearer ' + data.access_token;
              this.access = true;
            } else {
              this.access = false;
            }
          });
  
          setTimeout(() => {
                observer.next(this.access);
            }, 500);
  
          setTimeout(() => {
                observer.complete();
            }, 1000);
  
  
        }, err => console.error(err));
      }
    }

    // comprobamos si el numbero de identidad ya existe

    /*
    isIdentifacation(identification: string) {
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      return this.http.post(AuthService.REGISTER_URL, JSON.stringify({ identification: identification }))
        .pipe(
          map((response: Response) => response.json()), catchError(this.handleError));
    }
    */
    public identidades: any;
    customer: customer[] = [];

    isIdentifacation(identification): Observable<any> {
      const httpOptions = {
        headers: new HttpHeaders({
        'Content-Type':  'application/json'
       })
      };
      return this.http.get('https://testbankapi.firebaseio.com/clients.json').pipe(
        map((response: any) => {
          console.log(response)
        })
      );
    }

    private handleError(error: any) {
        console.log(error);
        return Observable.throw(error.json());
    }

    // listamos usuarios

    getAllUsers(): Observable<any> {
      const httpOptions = {
        headers: new HttpHeaders({
        'Content-Type':  'application/json'
       })
      };
      return this.http.get(AuthService.REGISTER_URL, httpOptions).pipe(map((response: any) => response));
    }

    // Register
    public register(name, credentials): Observable<customer>{
      if (
        credentials.firstname === null ||
        credentials.lastname === null ||
        credentials.birthdate === null ||
        credentials.identification == null) {
        return Observable.throw('Favor llenar todos los campos requeridos');
      } else {
        return Observable.create(observer => {
  
          this.http.post<customer>(AuthService.REGISTER_URL, credentials)
          .pipe(map((response: any) => {
            const dataRes = response;
            console.log(dataRes);
            name = dataRes.name;
            console.log(name);
            this.storage.set('usuario', name);
          })).subscribe(responseData => {
            console.log(responseData);
          });

  
          observer.next(true);
          observer.complete();
        });
      }
    }
  
    // Get Token
    public getToken() {
      return this.token;
    }
  
    // Logout
    public logout() {
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
}

