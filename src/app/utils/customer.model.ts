import { Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

export interface customer {
    id: number,
    data:dataCustom[]
}

export interface dataCustom {
    birthdate?: string,
    firstname?: string,
    identification?: string,
    lastname?: string | number,
}

interface AsyncValidator extends Validator {
    validate(c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null>
}